# IDEA

```
class Api:
    queryset = None
    fields = None
    related = False

    def serialize(self):
        self.data = []

        for obj in list(self.queryset.values(*(self.related or self.queryset.model._meta.get_all_field_names()))):
            self.data.append(self.dehydrate(obj))

    def dehydrate(self, obj):
        return obj

    def __init__(self, **kwargs):
        for var in ('queryset', 'fields', 'related', ):
            if kwargs.has_key(var):
                self.queryset = kwargs[var]
        self.serialize()

    def as_json(self):
        return json.dumps(self.data)

class WorkerApi(Api):
    queryset = Worker.objects.order_by('login_suffix')[0:10]

```
